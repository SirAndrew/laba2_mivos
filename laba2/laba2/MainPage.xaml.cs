﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace laba2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        private void DecToBinClick(object sender, System.EventArgs e)
        {
            int num = 0;
            try
            {
                num = Int32.Parse(input_num.Text);
                Console.WriteLine(num);
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            string BinaryCode = Convert.ToString(num, 2);
            output_num.Text = BinaryCode;
        }

        private void BinToDecClick(object sender, System.EventArgs e)
        {
            string dec = "";
            try
            {
                dec = Convert.ToInt64(input_num.Text, 2).ToString();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            output_num.Text = dec;
        }

        private void DecToHexClick(object sender, System.EventArgs e)
        {
            int num = 0;
            try
            {
                num = Int32.Parse(input_num.Text);
                Console.WriteLine(num);
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            string hex = Convert.ToString(num, 16);
            output_num.Text = hex;
            
        }

        private void HexToDecClick(object sender, System.EventArgs e)
        {
            string hex;
            try
            {
                hex = Convert.ToInt64(input_num.Text, 16).ToString();
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            output_num.Text = hex;
        }
    }
}
